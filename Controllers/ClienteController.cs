﻿using clientes_front.Models;
using Microsoft.AspNetCore.Mvc;

namespace clientes_front.Controllers
{
    public class ClienteController : Controller
    {
        private string path = "http://localhost:5285/api/";
       
        public async Task<IActionResult> Guardar(Cliente cliente)
        {
            using (var client = new HttpClient())
            {
                DateTime localDate = DateTime.Now;
                cliente.FechaAlta = localDate;
                cliente.Estatus = "Activo";
                int edad = DateTime.Today.AddTicks(-cliente.FechaNacimiento.Ticks).Year - 1;
                cliente.Edad = edad;
                client.BaseAddress = new Uri(path);
                var result = await client.PostAsJsonAsync("clientes", cliente);
                string resultContent = await result.Content.ReadAsStringAsync();

                if (result.StatusCode== System.Net.HttpStatusCode.BadRequest)
                {
                    return View(cliente);
                }
                else
                {
                    return RedirectToAction("Lista");
                }
            }
          
        }

        public IActionResult Lista()
        {
            IEnumerable<Cliente> clientes = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(path);
                var responseTask = client.GetAsync("clientes");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Cliente>>();
                    readTask.Wait();
                    clientes = readTask.Result;
                }
                else 
                {                  
                    clientes = Enumerable.Empty<Cliente>();
                    ModelState.AddModelError(string.Empty, "Error al comunicarse con el servidor, favor de comunicarse con el proveedor.");
                }
            }

            MiltiModel miltiModel = new MiltiModel();
            miltiModel.ClientesList = clientes;
            return View(miltiModel);
        }

  
        public IActionResult Editar(int  id)
        {
            Cliente clientes = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(path);
                var responseTask = client.GetAsync("clientes/"+id);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Cliente>();
                    readTask.Wait();
                    clientes = readTask.Result;
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Error al comunicarse con el servidor, favor de comunicarse con el proveedor.");
                }
            }
            return View(clientes);
        }

        [HttpPost]
        public async Task<IActionResult> Editar(Cliente cliente)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(path);
                if (cliente.FechaBaja != null)
                {
                    cliente.Estatus = "Inactivo";
                }
                var result = await client.PutAsJsonAsync("clientes", cliente);
                string resultContent = await result.Content.ReadAsStringAsync();
            }
            return RedirectToAction("Lista");
        }

        [HttpPost()]
        public async Task<IActionResult> EliminarAsync(int id)
        {

            Cliente clientes = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(path);
                var responseTask = client.GetAsync("clientes/" + id);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Cliente>();
                    readTask.Wait();
                    clientes = readTask.Result;
                   
                    DateTime localDate = DateTime.Now;
                    clientes.FechaBaja = localDate;
                    clientes.Estatus = "Inactivo";
                    var resultEdit = await client.PutAsJsonAsync("clientes", clientes);
                    string resultContentEdit = await result.Content.ReadAsStringAsync();

                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Error al comunicarse con el servidor, favor de comunicarse con el proveedor.");
                }
            }
             
           
            return RedirectToAction("Lista");
        }

        [HttpPost]
        public IActionResult Lista(ClienteFiltroDTO clienteFiltroDTO)
        {
            IEnumerable<Cliente> clientes = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(path);
                var responseTask = client.PostAsJsonAsync("clientes/getFilter",clienteFiltroDTO);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Cliente>>();
                    readTask.Wait();
                    clientes = readTask.Result;
                }
                else
                {
                    clientes = Enumerable.Empty<Cliente>();
                    ModelState.AddModelError(string.Empty, "Error al comunicarse con el servidor, favor de comunicarse con el proveedor.");
                }
            }

            MiltiModel miltiModel = new MiltiModel();
            miltiModel.ClientesList = clientes;
            return View(miltiModel);
        }
    }
}
