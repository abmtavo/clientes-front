﻿namespace clientes_front.Models
{
    public class ClienteFiltroDTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

        public string RFC { get; set; }

        public string Estatus { get; set; }
    }
}
